package org.plat.flowops.nova.database.schema

import org.plat.flowops.nova.database.MyPostgresProfile.api.*

import java.sql.Timestamp

case class NovaRepository(
    owner_id: Long,
    repository_name: String,
    repository_id: Option[Long],
    created_at: Timestamp,
    updated_at: Timestamp,
    isPrivate: Boolean,
    storage_path: Option[String],
    fl_repository_path: String
)

class NovaRepositoryTable(tag: Tag) extends Table[NovaRepository](tag, Some("nova"), "repository"):
  override def * = (
    owner_id,
    repository_name,
    repository_id.?,
    created_at,
    updated_at,
    isPrivate,
    storage_path,
    fl_repository_path
  ) <> ((NovaRepository.apply _).tupled, NovaRepository.unapply)

  def repository_id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def created_at = column[Timestamp]("created_at")

  def updated_at = column[Timestamp]("updated_at")

  def isPrivate = column[Boolean]("isPrivate")

  def owner_id = column[Long]("owner_id")

  def repository_name = column[String]("repository_name")

  def storage_path = column[Option[String]]("storage_path")

  def fl_repository_path = column[String]("fl_repository_path", O.Unique)

  def fl_repository_path_index = index("fl_repository_path_index", fl_repository_path, unique = true)
