package org.plat.flowops.nova.constants

object InternalConstants:
  val LOCKED_REPOSITORY_KEY     = "locked_repository"
  val DATABASE_TRANSACTION_KEY  = "database_transaction"
  val UPDATING_REPOSITORY_KEY   = "updating_repository"
  val USER_KEY                  = "user"
  val REPOSITORY_KEY            = "repository"
  val DEFAULT_COMMIT_PAGE_COUNT = 25
