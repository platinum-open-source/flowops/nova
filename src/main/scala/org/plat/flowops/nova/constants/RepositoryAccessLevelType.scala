package org.plat.flowops.nova.constants

enum RepositoryAccessLevelType:
  case READ, WRITE
