package org.plat.flowops.nova.tasks.rpc

import com.typesafe.scalalogging.LazyLogging
import io.grpc.{ Server, ServerBuilder }
import nova.repository_service.RepositoryServiceGrpc
import org.plat.flowops.nova.constants.DefaultEnvironmentConstants.RPC_PORT
import org.plat.flowops.nova.rpc.RepositoryServiceImpl
import org.plat.flowops.nova.tasks.Task
import org.plat.flowops.nova.tasks.injector.InjectorHandler
import org.plat.flowops.nova.utils.EnvironmentLoader

import scala.concurrent.{ ExecutionContext, Future }

object RpcMainRunner extends Task:
  def run(): Unit =
    val server = new RpcMainRunner(scala.concurrent.ExecutionContext.global)
    server.start()
    server.blockThreadUntilShutDown()

  override def execute(): Unit = new Thread(this).start()

private class RpcMainRunner(ec: ExecutionContext) extends LazyLogging:
  private[this] var server: Server = _

  private def start(): Unit =
    val port: Int = EnvironmentLoader.getEnvironmentVariable("RPC_PORT", RPC_PORT).toInt
    server = ServerBuilder
      .forPort(port)
      .addService(
        RepositoryServiceGrpc
          .bindService(InjectorHandler.getInjector.getInstance(classOf[RepositoryServiceImpl]), ec)
      )
      .build
      .start
    logger.info("Rpc Server started, listening on: " + port)

  private def stop(): Unit =
    if server != null then server.shutdown()

  private def blockThreadUntilShutDown(): Unit =
    if server != null then server.awaitTermination()
