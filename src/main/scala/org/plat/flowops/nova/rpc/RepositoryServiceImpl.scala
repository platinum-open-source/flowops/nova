package org.plat.flowops.nova.rpc

import com.google.inject.Inject
import com.typesafe.scalalogging.LazyLogging
import io.grpc.{ Status, StatusException }
import nova.errors.EmptyRepositoryError
import nova.repository_service.{
  CreateRepositoryRequest,
  CreateRepositoryResponse,
  GetFileListRequest,
  GetFileListResponse,
  RepositoryExistsRequest,
  RepositoryExistsResponse,
  RepositoryServiceGrpc
}

import scala.concurrent.ExecutionContext.Implicits.global
import org.plat.flowops.nova.database.schema.NovaRepository
import org.plat.flowops.nova.helper.RepositoryFactory
import org.plat.flowops.nova.service.{ RepositoryService, SystemService }

import java.sql.Timestamp
import java.time.Instant
import scala.concurrent.Future

class RepositoryServiceImpl @Inject (repositoryService: RepositoryService)
    extends RepositoryServiceGrpc.RepositoryService
    with SystemService
    with RepositoryFactory
    with LazyLogging:
  override def repositoryExists(request: RepositoryExistsRequest): Future[RepositoryExistsResponse] =
    Database()
      .run(
        repositoryService.existsByFlPath(request.repository.get.flRepositoryPath)
      )
      .flatMap { exists =>
        Future.successful(RepositoryExistsResponse(exists))
      }

  override def createRepository(request: CreateRepositoryRequest): Future[CreateRepositoryResponse] =
    val time = Timestamp.from(Instant.ofEpochMilli(request.createdAt))
    Database()
      .run(
        repositoryService.createRepository(
          NovaRepository(
            request.ownerId,
            request.repositoryName,
            Some(request.id),
            time,
            time,
            request.isPrivate,
            None,
            request.flRepositoryPath
          )
        )
      )
      .flatMap { i =>
        logger.debug(s"Creating repository: ${request.flRepositoryPath}")
        Future.successful(CreateRepositoryResponse(i == 1))
      }
      .recoverWith { e =>
        logger.error(s"Failed to create repository instance in database: ${e.getMessage}")
        Future.failed(
          new StatusException(
            Status.INTERNAL.withDescription("Failed to create repository instance in database")
          )
        )
      }

  override def getFileList(request: GetFileListRequest): Future[GetFileListResponse] =
    Database().run(getRepositoryById(request.repositoryId)).flatMap {
      case Some(repository) =>
        if repository.storage_path.isEmpty then
          Future.successful(
            GetFileListResponse().withEmptyRepositoryError(
              EmptyRepositoryError(flRepositoryPath = repository.fl_repository_path)
            )
          )
        ???
      case None =>
        logger.debug(s"Repository with id ${request.repositoryId} does not exist")
        Future.failed(
          new StatusException(
            Status.NOT_FOUND.withDescription("Repository does not exist")
          )
        )
    }
