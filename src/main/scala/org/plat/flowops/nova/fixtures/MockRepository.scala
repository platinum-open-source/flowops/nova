package org.plat.flowops.nova.fixtures
import org.plat.flowops.nova.database.MyPostgresProfile.MyAPI.queryInsertActionExtensionMethods
import org.plat.flowops.nova.database.schema.{ NovaRepository, NovaRepositoryTable }
import slick.lifted.TableQuery

import java.sql.Timestamp
import scala.concurrent.{ ExecutionContext, Future }

class MockRepository extends Fixture:

  private val time = System.currentTimeMillis()

  private val mockRepositories = List(
    NovaRepository(
      1L,
      "repo1",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "root/repo1"
    ),
    NovaRepository(
      1L,
      "repo2",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "root/repo2"
    ),
    NovaRepository(
      1L,
      "repo3",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "root/repo3"
    ),
    NovaRepository(
      1L,
      "repo4",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "root/repo4"
    ),
    NovaRepository(
      2L,
      "repo5",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "abh80/repo5"
    ),
    NovaRepository(
      2L,
      "repo6",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "abh80/repo6"
    ),
    NovaRepository(
      2L,
      "repo7",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "abh80/repo7"
    ),
    NovaRepository(
      2L,
      "repo8",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "abh80/repo8"
    ),
    NovaRepository(
      2L,
      "repo9",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "abh80/repo9"
    ),
    NovaRepository(
      1L,
      "repo10",
      None,
      new Timestamp(time),
      new Timestamp(time),
      isPrivate = false,
      None,
      "root/repo10"
    )
  )

  override def generate()(implicit ec: ExecutionContext): Future[?] =
    val repositories = table()
    Database().run(repositories ++= mockRepositories)

  override def table(): TableQuery[NovaRepositoryTable] = TableQuery[NovaRepositoryTable]
