package org.plat.flowops.nova.git

import org.eclipse.jgit.lib.ObjectId

class FileObject(
    var name: String,
    var description: String = "",
    var path: String = "",
    var isDirectory: Boolean = false,
    var commitDate: Long = 0L,
    var children: Seq[FileObject] = Seq(),
    private var objectId: ObjectId = null
):
  def this() =
    this("")

  def withName(name: String): FileObject =
    this.name = name
    this

  def withDescription(description: String): FileObject =
    this.description = description
    this

  def withPath(path: String): FileObject =
    this.path = path
    this

  def withIsDirectory(isDirectory: Boolean): FileObject =
    this.isDirectory = isDirectory
    this

  def withCommitDate(commitDate: Long): FileObject =
    this.commitDate = commitDate
    this

  def withObjectId(objectId: ObjectId): FileObject =
    this.objectId = objectId
    this

  def getObjectId: ObjectId = objectId

  def withChildren(children: Seq[FileObject]): FileObject =
    this.children = children
    this
