package org.plat.flowops.nova.git

import com.typesafe.scalalogging.LazyLogging
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.{ Constants, ObjectId, Repository }
import org.eclipse.jgit.revwalk.{ RevCommit, RevWalk }
import org.eclipse.jgit.treewalk.TreeWalk
import org.eclipse.jgit.treewalk.filter.{ AndTreeFilter, PathFilter, PathFilterGroup, TreeFilter }

import scala.jdk.CollectionConverters.*
import java.io.{ File, IOException }
import java.util.concurrent.Executors
import scala.collection.mutable
import scala.concurrent.{ ExecutionContext, ExecutionContextExecutorService, Future }
import scala.util.Using
import scala.util.Using.Releasable

class GitFileListProvider(repository: Repository) extends LazyLogging:
  private val executorService: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))

  private implicit val ec: ExecutionContext = executorService

  private var revId: ObjectId = _

  def getRepositoryHeadFiles(): Future[Seq[FileObject]] =
    Future {
      val treeWalk = buildTree()
      val parent   = FileObject().withName("").withPath("").withIsDirectory(true)
      findChildren(parent, treeWalk)
      parent.children
    }

  private def buildTree(): TreeWalk =
    Using.Manager { use =>
      val git      = use(new Git(repository))
      val revWalk  = use(new RevWalk(repository))
      val treeWalk = use(new TreeWalk(repository))

      val lastCommitObjectId = repository.resolve(Constants.HEAD)
      revId = lastCommitObjectId
      val headCommit = revWalk.parseCommit(lastCommitObjectId)
      val tree       = headCommit.getTree
      treeWalk.addTree(tree)
      treeWalk
    }.get

  private def convertToFileObject(
      parent: FileObject,
      treeEntries: Seq[TreeEntry]
  ): FileObject =
    val files = treeEntries.map(getFileObject)
    parent.withChildren(files)

  private def findChildren(parent: FileObject, treeWalk: TreeWalk): FileObject =
    val treeEntry = new TreeEntry()
    createTree(treeEntry, treeWalk)
    convertToFileObject(parent, treeEntry.getChildren)

  private def getFileObject(treeEntry: TreeEntry): FileObject =
    val file = FileObject().withName(treeEntry.name).withPath(treeEntry.path).withObjectId(treeEntry.objectId)
    val objectLoader = repository.open(treeEntry.objectId)
    file.withIsDirectory(objectLoader.getType == Constants.OBJ_TREE)
    updateCommitDetails(file)
    file

  private def updateCommitDetails(fileObject: FileObject): Unit =
    repository.incrementOpen()
    Using.Manager { use =>
      val _    = use(repository)
      val walk = use(new RevWalk(repository))

      walk.setTreeFilter(AndTreeFilter.create(TreeFilter.ANY_DIFF, PathFilter.create(fileObject.path)))
      val commit = walk.parseCommit(revId)

      walk.markStart(commit)
      val latestCommit = walk.iterator().next()

      applyCommitsToFile(fileObject, latestCommit)
    }

  private def applyCommitsToFile(file: FileObject, commit: RevCommit): Unit =
    file.withDescription(commit.getShortMessage)
    file.withCommitDate(commit.getCommitTime)

  private def createTree(parent: TreeEntry, treeWalk: TreeWalk): Unit =
    val parents: mutable.Queue[TreeEntry] = mutable.Queue(parent)

    while treeWalk.next() do
      val currentPath       = treeWalk.getPathString
      val currentParentPath = withTrailingSlash(parents)

      while !currentPath.startsWith(currentParentPath) do parents.dequeue()

      val currentParent = parents.front
      val treeEntry     = createTreeEntry(treeWalk)
      treeEntry.foreach(currentParent.addChild)

  private def createTreeEntry(treeWalk: TreeWalk): Option[TreeEntry] =
    val path     = treeWalk.getPathString
    val objectId = treeWalk.getObjectId(0)

    if repository.getObjectDatabase.has(objectId) then
      val typeStruct = if treeWalk.isSubtree then TreeEntryType.Directory else TreeEntryType.File
      Some(TreeEntry(path, treeWalk.getNameString, objectId, typeStruct))
    else None

  private class TreeEntry(
      val path: String,
      val name: String,
      val objectId: ObjectId,
      val typeStruct: TreeEntryType
  ):
    def this() =
      this("", "", null, TreeEntryType.Directory)

    private val children: mutable.ArrayBuffer[TreeEntry] = mutable.ArrayBuffer()

    def getChildren: Seq[TreeEntry] =
      children.sortBy(node => (node.typeStruct == TreeEntryType.File, node.name)).toSeq

    def addChild(entry: TreeEntry): Unit =
      children.addOne(entry)

  private enum TreeEntryType:
    case File
    case Directory

  @throws[IOException]
  private def withTrailingSlash(parents: mutable.Queue[TreeEntry]): String =
    val path = parents.head.path
    if path.isEmpty then path else path + "/"
