package org.plat.flowops.nova.helper

import com.typesafe.scalalogging.LazyLogging
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.eclipse.jgit.lib.Repository
import org.plat.flowops.nova.service.{ RepositoryService, SystemService }

import java.io.File
import java.nio.file.Paths
import java.security.MessageDigest
import scala.concurrent.{ ExecutionContext, Future }

trait RepositoryFactory(implicit ec: ExecutionContext) extends RepositoryService with LazyLogging:
  def getDiskRepository(
      fl_repository_path: String,
      storage_path: Option[String],
      repository_id: Long
  ): Future[Option[Repository]] =
    if storage_path.isEmpty then
      val git_storage_path = new File(
        getHashedStoragePath(fl_repository_path = fl_repository_path, basePath = DIRECTORY.REPO_DIR)
      )
      logger.debug(s"Getting repository: ${git_storage_path.getAbsolutePath}")
      logger.debug("Updating storage value in database")

      Database()
        .run(setRepositoryStoragePath(repository_id, git_storage_path.getAbsolutePath))
        .flatMap { i =>
          initGitRepository(git_storage_path)
          Future.successful(Some(new FileRepository(git_storage_path)))
        }
        .recoverWith { e =>
          logger.error(s"Error updating storage path in database for path: $fl_repository_path")
          Future.successful(None)
        }
    else
      logger.debug(s"Getting repository: ${storage_path.get}")
      Future.successful(Some(new FileRepository(storage_path.get)))

  private def initGitRepository(dir: File): Unit =
    logger.debug(s"Initializing an empty git repository: ${dir.getAbsolutePath}")

    if !dir.exists() then
      logger.debug(s"Repository directory does not exist, creating one...")
      if !dir.mkdirs() then
        logger.error(s"Failed to create repository directory: ${dir.getAbsolutePath}")
        throw new RuntimeException(s"Failed to create repository directory: ${dir.getAbsolutePath}")
      else
        Git
          .init()
          .setBare(true)
          .setDirectory(dir)
          .call()
          .getRepository

  private def sha256(fl_repository_path: String): String =
    val digest = MessageDigest.getInstance("SHA-256")
    digest.digest(fl_repository_path.getBytes("UTF-8")).map("%02x".format(_)).mkString

  private def getHashedStoragePath(fl_repository_path: String, basePath: String): String =
    val hash = sha256(fl_repository_path)
    Paths.get(basePath, "@hashed", hash.substring(0, 2), hash.substring(2, 4), s"${hash}.git").toString
